#include <iostream>
#include <stdio.h>


using namespace std;

int main ()
{
	char kalimat[50];
	cout << "Input word: " ;
	gets (kalimat);
	
	cout << "Vowel Characters : ";
	for (int i = 0; i < strlen(kalimat); i++)
	{
		if (kalimat[i] == 'A' || kalimat[i] == 'a' || 
			kalimat[i] == 'E' || kalimat[i] == 'e' || 
			kalimat[i] == 'I' || kalimat[i] == 'i' || 
			kalimat[i] == 'U' || kalimat[i] == 'u' || 
			kalimat[i] == 'O' || kalimat[i] == 'o')
		{
			cout << kalimat[i] << " ";
		}
	}

    cout << "\nConsonant Characters : ";
    	for (int i = 0; i < strlen(kalimat); i++ )
    	{
			if (kalimat[i] == 'A' || kalimat[i] == 'a' || 
			kalimat[i] == 'E' || kalimat[i] == 'e' || 
			kalimat[i] == 'I' || kalimat[i] == 'i' || 
			kalimat[i] == 'U' || kalimat[i] == 'u' || 
			kalimat[i] == 'O' || kalimat[i] == 'o')
			{
				continue;
			}
			else
			{
				if (isspace(kalimat[i]))
				{
					continue;
				}
				else
				{
					cout << kalimat[i] << " ";
				}
			}
    	}
	
	cin.get();
	return 0;
}