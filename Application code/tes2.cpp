#include <iostream>

using namespace std;

int main ()
{
	int a, n, b;
	cout << "Masukkan nilai awal = ";
	cin >> a;
	cout << "Masukkan batas = ";
	cin >> n;
	cout << "Masukkan beda deret = ";
	cin >> b;

	for (int i = a; i <= n; i += b)
	{
		cout << i << " ";
	}
	return 0;
}